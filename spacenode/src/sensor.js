import Gpio from 'onoff'

const StateEnum = {
	free: 1, 
	check: 2, 
	taken: 3
}
Object.freeze(StateEnum)

class Sensor {
    constructor(gpioNo, movementThreshold) {
        this.gpioNo = gpioNo
        this.movementThreshold = movementThreshold
        this.startWatcher()
    }

    latestStatus = {
        latestMovement: null,
        hasMovement: false
    }
    publishStatus = this.latestStatus

    startWatcher() {
        var switchIn = new Gpio.Gpio(this.gpioNo, 'in', 'both')
        // Listen to changes in the sensor
        switchIn.watch((err, value) => {
            if (err) {
                console.log('Error', err)
            }

            this.latestStatus = {
                latestMovement: this.latestStatus.latestMovement,
                hasMovement: value == 0
            }

            // Movement sensed, publish this status immediately
            if (this.latestStatus.hasMovement) {
                this.latestStatus.latestMovement = Date.now()
                this.publishStatus = this.latestStatus
                this.onChange(this.publishStatus)
            }
        })

        // Check at threshold intervals if we should publish
        setInterval(() => {
            // console.log(this.latestStatus)
            // console.log(this.publishStatus)
            var msSinceLatestMovement = Date.now() - this.latestStatus.latestMovement

            // If more than threshold amount of time has passed since last movement, update the status
            if (msSinceLatestMovement > this.movementThreshold
                && this.latestStatus.latestMovement != null
                && this.publishStatus != this.latestStatus
            ) {
                this.publishStatus = this.latestStatus
                this.onChange(this.publishStatus)
            }
        }, this.movementThreshold / 10)
    }

    onChange = (status) => { }
}

export default Sensor