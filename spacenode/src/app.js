import express from 'express'
import { readFileSync } from 'fs'
import WebSocket from 'ws'

const config = JSON.parse(readFileSync('./config.json'))
const app = express()
const wss = new WebSocket.Server({port: 2140, path: '/status'})

import Sensor from './sensor.js'
const sensor = new Sensor('24', config.movementThreshold)

// Broadcast to all.
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        console.log('IT IS GETTING INSIDE CLIENTS');
        console.log(client);
        
        // The data is coming in correctly
        console.log(data);
        client.send(data);
    });
};

const sendWSUpdate = (status) => {
	wss.broadcast(JSON.stringify(status))
}

sensor.onChange = sendWSUpdate

wss.on('connection', (ws) => {
	console.log('ws connected')
	ws.on('message', (msg) => {
		console.log('received %s', msg)
	})
	ws.on('close', () => {
		console.log('ws disconnected')
	})
	ws.send(JSON.stringify(sensor.publishStatus))
})

app.get('/status', (req, res) => res.send(sensor.publishStatus))

app.listen(1240, () => console.log('SpaceNode listening on port 1240'))
