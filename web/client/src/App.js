import React from 'react';
import './App.css';
import SpaceNode from './SpaceNode';

function App() {
  return (
    <div className="App">
      <h1>SpaceJam</h1>
      <SpaceNode host="192.168.10.240" http_port="1240" ws_port="2140" name="Main meeting room"/>
    </div>
  );
}

export default App;
