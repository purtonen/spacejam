import React from 'react';
import './SpaceNode.css';

class SpaceNodeComponent extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            latestMovement: null,
            hasMovement: false
        }
    }
    components = {
        spaceNode: SpaceNode
    }
    ws = new WebSocket(`ws://${this.props.host}:${this.props.ws_port}/status`)
    componentDidMount() {
        console.log(`ws://${this.props.host}:${this.props.ws_port}/status`)
        this.ws.onopen = () => {
            console.log('SpaceNode connected to WebSocket on %s', this.props.host)
        }
        this.ws.onmessage = (msg) => {
            console.log(msg)
            let status = JSON.parse(msg.data)
            this.setState(status)
        }
    }
    render() {
        let statusHTML
        let latestHTML
        if (this.state.latestMovement != null) {
            let options = {
                day: 'numeric',
                month: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric',
                timeStyle: 'full'
            }
            let date = new Intl.DateTimeFormat('fi-FI', options).format(this.state.latestMovement)
            latestHTML = <span>(Latest movement: {date})</span>
        }
        if (this.state.hasMovement) {
            statusHTML = <p>Status: TAKEN {latestHTML}</p>
        }
        else {
            statusHTML = <p>Status: FREE {latestHTML}</p>
        }
        const SpaceNode = this.components[this.props.tag]
        return (
            <SpaceNode className={this.state.hasMovement ? 'taken' : 'free'}>
                <h2>SpaceNode</h2>
                <div>
                    <p>HTTP: {this.props.host}:{this.props.http_port}</p>
                    <p>WS: {this.props.host}:{this.props.ws_port}</p>
                    {statusHTML}
                </div>
            </SpaceNode>
        );
    }
}

export default SpaceNodeComponent