import React from 'react';
import './SpaceNode.scss';

class SpaceNodeComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            latestMovement: null,
            hasMovement: false,
            wsConnected: false
        }
    }

    ws = null

    componentDidMount() {
        this.wsSetup()
    }

    wsSetup() {
        this.ws = new WebSocket(`ws://${this.props.host}:${this.props.ws_port}/status`)

        this.ws.onopen = () => {
            console.log('SpaceNode connected to WebSocket on %s', this.props.host)
            let state = this.state
            state.wsConnected = true
            this.setState(state)
        }
        this.ws.onmessage = (msg) => {
            console.log(msg)
            let status = JSON.parse(msg.data)
            let state = this.state
            state.latestMovement = status.latestMovement
            state.hasMovement = status.hasMovement
            this.setState(state)
        }
        this.ws.onclose = () => {
            console.log('ws connection closed')
            let state = this.state
            state.wsConnected = false
            this.setState(state)
            setTimeout(() => {
                this.wsSetup()
            }, 5000)
        }
    }

    render() {
        let statusHTML
        let latestHTML
        if (this.state.latestMovement != null) {
            let options = {
                day: 'numeric',
                month: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric',
                timeStyle: 'full'
            }
            let date = new Intl.DateTimeFormat('fi-FI', options).format(this.state.latestMovement)
            latestHTML = <span><br/>(Latest movement: {date})</span>
        }
        if (this.state.hasMovement && this.state.wsConnected) {
            statusHTML = <p>Status: TAKEN {latestHTML}</p>
        }
        else if (this.state.wsConnected) {
            statusHTML = <p>Status: FREE {latestHTML}</p>
        }
        else {
            statusHTML = <p>Status: UNKNOWN {latestHTML}</p>
        }

        return (
            <div className={`
            SpaceNode
            ${this.state.hasMovement && this.state.wsConnected ? 'taken' : ''}
            ${!this.state.hasMovement && this.state.wsConnected ? 'free' : ''}
            ${!this.state.wsConnected ? 'disabled' : ''}`}>
                <h2>{this.props.name}</h2>
                <div>
                    {/*<p>HTTP: {this.props.host}:{this.props.http_port}</p>*/}
                    {/*<p>WS: {this.props.host}:{this.props.ws_port}</p>*/}
                    {statusHTML}
                </div>
            </div>
        );
    }
}

export default SpaceNodeComponent